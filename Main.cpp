// string::length
#include <iostream>
#include <string>

int main()
{
	std::string str("Test string");
	std::cout << "The string: " << str << std::endl;
	std::cout << "The size of string is " << str.length() << " bytes." << std::endl;
	std::cout << "The first character is " << str.front() << std::endl;
	std::cout << "The last character is " << str.back() << std::endl;
	return 0;
}